require.config({
  urlArgs: "v=" +  Date.now(),
  paths: {
    'hogan' : 'vendor/hogan',
    'text' : 'vendor/text',
    'hgn' : 'vendor/hgn',
    'signals' : 'vendor/signals',
    'glmatrix' : 'vendor/glmatrix'
  },
  packages: [],
    shim: {}
});


define(['application'], function (Application) {

    var app = Application();

});
