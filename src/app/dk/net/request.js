define(function (require, exports, module) {

    var Request = function(url, type) {
        this.method = 'GET';
        this.request = new XMLHttpRequest();
        this.url = url;
        this.responseType = type || 'text';
        this.responseData = null;
        this.onComplete = null;
        this.cacheBust = false;
    };

    Request.prototype.send = function() {
        if (!this.url) {
            throw new Error("DKRequest requires a url property to be not null.");
        }
        var busted = (this.cacheBust) ? "?v="+new Date().getTime() : "";
        this.request.open(this.method, this.url+busted, true);

        this.request.onload = this.onRequestLoaded.bind(this);
        this.request.onreadystatechange = this.onStateChange.bind(this);

        this.request.send();
    };

    Request.prototype.onRequestLoaded = function(progressEvent) {

    };

    Request.prototype.onStateChange = function() {
        if (this.request.readyState==4) {
            if (this.responseType=='json') {
                this.responseData = JSON.parse(this.request.responseText);
            } else {
                this.responseData = this.request.responseText;
            }
            if (this.onComplete) this.onComplete(this, this.responseData);
        }
    };

    module.exports = Request;
});
