define(function (require, exports, module) {



    var Motion = function() {
        this.bindOrientationHandler = this.orientationHandler.bind(this);
        if (window.DeviceOrientationEvent) {
            console.log("DeviceOrientation is supported");
        }
    };


    Motion.prototype.startCapture = function () {
        window.addEventListener('deviceorientation', this.bindOrientationHandler, false);
    };

    Motion.prototype.orientationHandler = function( data ) {
        console.log(data);
        
    };


    module.exports = Motion;
});