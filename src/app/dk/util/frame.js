define(function (require, exports, module) {
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelAnimationFrame =
          window[vendors[x]+'CancelAnimationFrame'] || window[vendors[x]+'CancelRequestAnimationFrame'];
    }

    if (!window.requestAnimationFrame) {
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() { callback(currTime + timeToCall); },
              timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };
    }

    if (!window.cancelAnimationFrame) {
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
    }

    var signals = require('signals');
    var globalID = null;
    var timeoutID = null;

    var Frame = {};
    Frame.fps = 60;

    Frame.update = new signals.Signal();

    Frame.loop = function() {
        Frame.update.dispatch();
        timeoutID = setTimeout(function(){
            globalID = requestAnimationFrame(Frame.loop);
        }, 1000 / Frame.fps);
        
    };

    Frame.play = function() {
        globalID = requestAnimationFrame(Frame.loop);
    };

    Frame.pause = function() {        
        cancelAnimationFrame(globalID);
        clearTimeout(timeoutID);
    };

    module.exports = Frame;
});


