
define(function (require, exports, module) {

    var signals = require('signals');

    var _wheelSpeed = 0.1;
    var _value = 0;
    var _target = null;
    var _child = null;
    var _scrollHeight = 0;
    var _contentSize = 0;
    var _ticking = false;
    var _minLimit = 0;
    var _maxLimit = 0;
    var _deltaValue = 0;
    //var _offset = 0;


    var Scroller = function(target) {
        this.onUpdate = new signals.Signal();
        this.target = target;
        this.child = target.firstChild;

        this.contentSize = this.child.offsetHeight;
        this.minLimit = target.offsetHeight - this.contentSize;

        this.value = 0;
        this.easingValue = 0;
        this.speed= 0;
        this.prevPosition = 0;

        _target.onmousewheel = this.onMouseWheel.bind(this);
    };

    Scroller.prototype.updateHeight = function(height) {
      _contentSize = height;
    };

    Scroller.prototype.setWheelSpeed = function(value) {
      _wheelSpeed = value;
    };

    Scroller.prototype.onMouseWheel = function(event) {
      event.preventDefault();
      //console.log(event);
      this.speed = event.wheelDeltaY * _wheelSpeed;
      if(Math.abs(this.speed) < 1) this.speed = 0;

      this.requestTick();
    };

    Scroller.prototype.requestTick = function() {
        //console.log(this.update);
        if (!_ticking) {
            requestAnimationFrame(this.update.bind(this));
            _ticking = true;
        }
    };

    Scroller.prototype.update = function() {
      _minLimit = _target.offsetHeight - _contentSize;
      _value = this.easingValue;

      this.speed *= 0.9;
      this.easingValue += this.speed;
      if (this.easingValue <= _minLimit) this.easingValue = _minLimit;
      if (this.easingValue >= _maxLimit) this.easingValue = _maxLimit;
      if(Math.abs(this.speed) < 1) this.speed = 0;
      _ticking=false;
      if (this.onUpdate) this.onUpdate.dispatch(_value);
    };

    module.exports =  Scroller;
});